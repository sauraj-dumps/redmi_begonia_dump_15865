## begonia-user 10 QP1A.190711.020 V12.0.8.0.QGGMIXM release-keys
- Manufacturer: xiaomi
- Platform: mt6785
- Codename: begonia
- Brand: Redmi
- Flavor: begonia-user
- Release Version: 10
- Id: QP1A.190711.020
- Incremental: V12.0.8.0.QGGMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/begonia/begonia:10/QP1A.190711.020/V12.0.8.0.QGGMIXM:user/release-keys
- OTA version: 
- Branch: begonia-user-10-QP1A.190711.020-V12.0.8.0.QGGMIXM-release-keys
- Repo: redmi_begonia_dump_15865


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
